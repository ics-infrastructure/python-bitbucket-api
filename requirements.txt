certifi==2017.4.17
click==6.7
oauthlib==2.0.2
PyYAML==3.12
requests==2.13.0
requests-oauthlib==0.8.0
