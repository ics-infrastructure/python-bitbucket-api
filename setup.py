from setuptools import setup


setup(name='bitbucket',
      version='0.3.2',
      description='Python script to interact with Bitbucket API',
      url='https://bitbucket.org/europeanspallationsource/bitbucket',
      author='Benjamin Bertrand',
      author_email='benjamin.bertrand@esss.se',
      license='MIT',
      py_modules=['bitbucket'],
      install_requires=[
          'Click',
          'PyYAML',
          'requests',
          'requests-oauthlib',
      ],
      entry_points='''
          [console_scripts]
          bitbucket=bitbucket:cli
      ''',
      setup_requires=['pytest-runner'],
      tests_require=[
          'pytest',
          'pytest-catchlog',
          'requests-mock',
      ]
      )
