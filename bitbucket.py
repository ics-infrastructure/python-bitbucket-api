import os
import click
import logging
import sys
import yaml
from oauthlib.oauth2 import BackendApplicationClient
from requests_oauthlib import OAuth2Session

try:
    # Python 3
    from urllib.parse import urlencode
except ImportError:
    # Python 2
    from urllib import urlencode


BITBUCKET_API_URL = 'https://api.bitbucket.org/2.0'
BITBUCKET_TOKEN_URL = 'https://bitbucket.org/site/oauth2/access_token'
logger = logging.getLogger(__name__)
handler = logging.StreamHandler()
formatter = logging.Formatter('%(asctime)s %(name)-12s %(levelname)-8s %(message)s')
handler.setFormatter(formatter)
logger.addHandler(handler)
logger.setLevel(logging.INFO)


class BitbucketClient:

    def __init__(self, conf):
        with open(conf, 'r') as f:
            config = yaml.load(f)
        self.bitbucket = config['bitbucket']
        self.webhooks = config.get('webhooks', [])
        client = BackendApplicationClient(client_id=self.bitbucket['client_id'])
        self.oauth = OAuth2Session(client=client)
        self.oauth.fetch_token(token_url=BITBUCKET_TOKEN_URL,
                               client_id=self.bitbucket['client_id'],
                               client_secret=self.bitbucket['client_secret'])

    def pages(self, url, params=None):
        """Generator that returns all pages from a paginated API"""
        r = self.oauth.get(url, params=params)
        r.raise_for_status()
        if r.status_code == 200:
            data = r.json()
            yield data
            while 'next' in data:
                r = self.oauth.get(data['next'])
                r.raise_for_status()
                data = r.json()
                yield data

    def get_repo_slugs(self, project_key):
        """Return all repositories slug part of the project"""
        url = '{api_url}/repositories/{owner}'.format(
            api_url=BITBUCKET_API_URL, owner=self.bitbucket['owner'])
        for page in self.pages(url, params={'q': 'project.key="{}"'.format(project_key)}):
            for repo in page['values']:
                # Temporary check
                if repo['project']['key'] != project_key:
                    logger.error('Invalid project key: {}'.format(repo['project']['key']))
                else:
                    yield repo['slug']

    def webhook(self, name, slug):
        """Return the webhook from the repository or None"""
        url = '{api_url}/repositories/{owner}/{slug}/hooks'.format(
            api_url=BITBUCKET_API_URL, owner=self.bitbucket['owner'], slug=slug)
        for page in self.pages(url):
            for webhook in page['values']:
                if webhook['description'] == name:
                    return webhook
        return None

    def webhook_from_conf(self, name):
        """Return the webhook information from the config"""
        for webhook in self.webhooks:
            if name == webhook['name']:
                return webhook
        return None

    def create_webhook(self, data, slug):
        url = '{api_url}/repositories/{owner}/{slug}/hooks'.format(
            api_url=BITBUCKET_API_URL, owner=self.bitbucket['owner'], slug=slug)
        r = self.oauth.post(url, json=data)
        if r.status_code == 201:
            logger.info('Webhook "{}" successfully created for {}'.format(
                data['description'], slug))
        else:
            logger.warning('Webhook "{}" NOT created for {}: {}'.format(data['description'], slug, r.text))

    def update_webhook(self, data, slug, uuid):
        url = '{api_url}/repositories/{owner}/{slug}/hooks/{uuid}'.format(
            api_url=BITBUCKET_API_URL, owner=self.bitbucket['owner'], slug=slug, uuid=uuid)
        r = self.oauth.put(url, json=data)
        if r.status_code == 200:
            logger.info('Webhook "{}" successfully updated for {}'.format(
                data['description'], slug))
        else:
            logger.warning('Webhook "{}" NOT updated for {}: {}'.format(data['description'], slug, r.text))

    def delete_webhook(self, slug, uuid):
        url = '{api_url}/repositories/{owner}/{slug}/hooks/{uuid}'.format(
            api_url=BITBUCKET_API_URL, owner=self.bitbucket['owner'], slug=slug, uuid=uuid)
        r = self.oauth.delete(url)
        if r.status_code == 204:
            logger.info('Webhook "{}" successfully deleted for {}'.format(
                uuid, slug))
        else:
            logger.warning('Webhook "{}" NOT deleted for {}: {}'.format(uuid, slug, r.text))

    def create_webhooks(self, name, repo_slugs, update=False):
        """Create a webhook on all repositories passed in parameters"""
        webhook_info = self.webhook_from_conf(name)
        if webhook_info is None:
            logger.error('Unknown webhook "{}". Check your configuration. Abort.'.format(name))
            sys.exit(1)
        query_string = urlencode(webhook_info['args'])
        data = {'description': name,
                'url': '{url}?{query_string}'.format(url=webhook_info['url'], query_string=query_string),
                'active': True,
                'events': ['repo:push'],
                }
        for slug in repo_slugs:
            if slug in webhook_info.get('exclude', []):
                logger.info('Skipping {}. Repository is excluded from {}.'.format(slug, name))
                continue
            webhook = self.webhook(name, slug)
            if webhook is not None:
                if update:
                    self.update_webhook(data, slug, webhook['uuid'])
                else:
                    logger.info('Skipping {}. Webhook "{}" already exists.'.format(slug, name))
            else:
                self.create_webhook(data, slug)

    def delete_webhooks(self, name, repo_slugs):
        """Delete the webhook on all repositories passed in parameters"""
        for slug in repo_slugs:
            webhook = self.webhook(name, slug)
            if webhook is None:
                logger.info("Skipping {}. Webhook '{}' doesn't exist.".format(slug, name))
            else:
                self.delete_webhook(slug, webhook['uuid'])

    def build_status(self, repo_slug, revision, state, key, build_url, name, description=None):
        """Update the build status of a commit"""
        url = '{api_url}/repositories/{owner}/{repo_slug}/commit/{revision}/statuses/build'.format(
            api_url=BITBUCKET_API_URL, owner=self.bitbucket['owner'], repo_slug=repo_slug, revision=revision)
        data = {'state': state,
                'key': key,
                'url': build_url,
                'name': name,
                }
        if description is not None:
            data['description'] = description
        r = self.oauth.post(url, json=data)
        if r.status_code in (200, 201):
            logger.info('Build status succesfully updated to {} for {} commit {}'.format(
                state, repo_slug, revision))
        else:
            logger.warning('Build status NOT updated for {} commit {}: {}'.format(
                repo_slug, revision, r.text))


@click.group()
@click.option('--conf', '-c', default='~/.bitbucket.yml',
              help='Configuration file [default: "~/.bitbucket.yml"]')
@click.option('--debug/--no-debug', default=False,
              help='Set log level to debug')
@click.version_option()
@click.pass_context
def cli(ctx, conf, debug):
    """Script to interact with Bitbucket API"""
    if debug:
        logger.setLevel(logging.DEBUG)
    filepath = os.path.expanduser(conf)
    if not os.path.exists(filepath):
        raise click.BadParameter("Configuration file '{}' doesn't exist".format(filepath))
    ctx.obj = BitbucketClient(filepath)


@cli.command()
@click.argument('name')
@click.option('--repo-slug', '-r', 'repo_slugs', multiple=True,
              help='Repository slug to update. Can be repeated. Webhook is only created for those repositories.')
@click.option('--project-key', '-p',
              help='Projet key. Webhook is updated for all repositories part of this project.')
@click.option('--delete', '-d', is_flag=True,
              help='Delete the webhook.')
@click.option('--update', '-u', is_flag=True,
              help='Update the webhook.')
@click.pass_obj
def webhook(bitbucket_client, name, repo_slugs, project_key, delete, update):
    """Modify a webhook on the specified repositories"""
    if not repo_slugs:
        repo_slugs = bitbucket_client.get_repo_slugs(project_key)
    if delete:
        bitbucket_client.delete_webhooks(name, repo_slugs)
    else:
        bitbucket_client.create_webhooks(name, repo_slugs, update)


@cli.command()
@click.argument('repo-slug')
@click.option('--revision', required=True,
              help='The SHA1 value for the commit where you want to add the build status.')
@click.option('--state', required=True,
              type=click.Choice(['INPROGRESS', 'SUCCESSFUL', 'FAILED']),
              help='Status of the build.')
@click.option('--key', required=True,
              help='A key to identify the submitted build status.')
@click.option('--url', required=True,
              help='URL for the system that produces the build.')
@click.option('--name', default='Jenkins EPICS deployment',
              help='Name of the build [default: "Jenkins EPICS deployment"].')
@click.option('--description', help='Description of the build.')
@click.pass_obj
def build_status(bitbucket_client, repo_slug, revision, state, key, url, name, description):
    """Update the build status of a commit"""
    bitbucket_client.build_status(repo_slug, revision, state, key, url, name, description)
